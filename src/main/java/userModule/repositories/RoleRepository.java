package userModule.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import userModule.models.Permission;
import userModule.models.Role;

public class RoleRepository extends Repository<Role> {
	private Connection connection;
	private Repository<Permission> permissionRepository;
	
	public RoleRepository(Connection connection, Repository<Permission> permissionRepository) {
		this.connection = connection;
		this.permissionRepository = permissionRepository;
	}
	
	public Integer persist(Role entity) throws RepositoryException {
		try (PreparedStatement statement = prepareInsert()) {
			statement.setString(1, entity.getName());
			statement.setInt(2, entity.getPersonId());
			statement.executeQuery();
			return getLastInsertId();
		} catch (Exception e) {
			throwSaveRepositoryException();
		}
		return null;
	}

	public String getTableName() {
		return "Role";
	}

	public Connection getConnection() {
		return connection;
	}

	protected void createTableIfNotExists() throws RepositoryException {
		try (Statement statement = connection.createStatement()) {
			statement.executeUpdate(String.format("CREATE TABLE IF NOT EXISTS %s (id INTEGER PRIMARY KEY, name VARCHAR, person_id INTEGER, FOREIGN KEY(person_id) REFERENCES Person(id))", getTableName()));
		} catch (SQLException ex) {
			throwCreateTableRepositoryException();
		}
	}

	protected Role buildEntity(ResultSet data) {
		Role role = new Role();
		try {
			role.setId(data.getInt("id"));
			role.setName(data.getString("name"));
			role.setPersonId(data.getInt("person_id"));
			role.setPermissions((ArrayList<Permission>) permissionRepository.getAllByCondition(String.format("role_id = %d", data.getInt("role_id"))));
		} catch (SQLException e) {
			// move along
		}
		return role;
	}
	
	private PreparedStatement prepareInsert() throws SQLException {
		return connection.prepareStatement(String.format("INSERT INTO %s(name, person_id) VALUES(?, ?)", getTableName()));
	}
}
