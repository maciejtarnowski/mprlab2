package userModule.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import userModule.models.Address;

public class AddressRepository extends Repository<Address> {
	private Connection connection;
	
	public AddressRepository(Connection connection) {
		this.connection = connection;
	}

	public Integer persist(Address entity) throws RepositoryException {
		try (PreparedStatement statement = prepareInsert()) {
			statement.setString(1, entity.getStreet());
			statement.setString(2, entity.getHouse());
			statement.setString(3, entity.getApartment());
			statement.setString(4, entity.getCity());
			statement.setString(5, entity.getPostcode());
			statement.setInt(6, entity.getPersonId());
			statement.executeQuery();
			return getLastInsertId();
		} catch (Exception e) {
			throwSaveRepositoryException();
		}
		return null;
	}

	protected void createTableIfNotExists() throws RepositoryException {
		try (Statement statement = connection.createStatement()) {
			statement.executeUpdate(String.format("CREATE TABLE IF NOT EXISTS %s (id INTEGER PRIMARY KEY, street VARCHAR, house VARCHAR, apartment VARCHAR, city VARCHAR, postcode VARCHAR, person_id INTEGER, FOREIGN KEY(person_id) REFERENCES Person(id))", getTableName()));
		} catch (SQLException ex) {
			throwCreateTableRepositoryException();
		}
	}
	
	public String getTableName() {
		return "Address";
	}

	public Connection getConnection() {
		return connection;
	}
	
	private PreparedStatement prepareInsert() throws SQLException {
		return connection.prepareStatement(String.format("INSERT INTO %s(street, house, apartment, city, postcode, person_id) VALUES(?, ?, ?, ?, ?, ?)", getTableName()));
	}
	
	protected Address buildEntity(ResultSet data) {
		Address address = new Address();
		try {
			address.setId(data.getInt("id"));
			address.setStreet(data.getString("street"));
			address.setHouse(data.getString("house"));
			address.setApartment(data.getString("apartment"));
			address.setCity(data.getString("city"));
			address.setPostcode(data.getString("postcode"));
			address.setPersonId(data.getInt("person_id"));
		} catch (SQLException e) {
			// move along
		}
		return address;
	}
}
