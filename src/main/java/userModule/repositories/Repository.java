package userModule.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

abstract public class Repository<Entity> {
	public List<Entity> getAllByCondition(String condition) {
		try (Statement statement = getConnection().createStatement()) {
			createTableIfNotExists();
			ResultSet results = selectAllFromTable(statement, condition);
			return buildAll(results);
		} catch (Exception e) {
			// move along
		}
		return new ArrayList<Entity>();
	}
	
	public List<Entity> getAll() {
		return getAllByCondition("");
	}
	
	public Entity getById(Integer id) {
		try (PreparedStatement statement = prepareSelectById()) {
			createTableIfNotExists();
			statement.setInt(1, id);
			return buildEntity(statement.executeQuery());
		} catch (Exception e) {
			return null;
		}
	}
	
	public Integer deleteById(Integer id) {
		try (PreparedStatement statement = prepareDeleteById()) {
			createTableIfNotExists();
			statement.setInt(1, id);
			return statement.executeUpdate();
		} catch (Exception e) {
			return 0;
		}
	}
	
	public Integer deleteAll() {
		try (Statement statement = getConnection().createStatement()) {
			createTableIfNotExists();
			return deleteAllFromTable(statement);
		} catch (Exception e) {
			return 0;
		}
	}
	
	abstract public Integer persist(Entity entity) throws RepositoryException;
	
	abstract public String getTableName();
	
	abstract public Connection getConnection();
	
	abstract protected void createTableIfNotExists() throws RepositoryException;
	
	abstract protected Entity buildEntity(ResultSet data);
	
	protected List<Entity> buildAll(ResultSet data) throws SQLException {
		ArrayList<Entity> entities = new ArrayList<Entity>();
		
		while (data.next()) {
			entities.add(buildEntity(data));
		}
		
		return entities;
	}
	
	protected PreparedStatement prepareSelectById() throws SQLException {
		return getConnection().prepareStatement(String.format("SELECT * FROM %s WHERE id = ?", getTableName()));
	}
	
	protected PreparedStatement prepareDeleteById() throws SQLException {
		return getConnection().prepareStatement(String.format("DELETE FROM %s WHERE id = ?", getTableName()));
	}
	
	protected Integer deleteAllFromTable(Statement statement) throws SQLException {
		return statement.executeUpdate(String.format("DELETE FROM %s", getTableName()));
	}
	
	protected ResultSet selectAllFromTable(Statement statement) throws SQLException {
		return selectAllFromTable(statement, "");
	}
	
	protected ResultSet selectAllFromTable(Statement statement, String condition) throws SQLException {
		return statement.executeQuery(String.format("SELECT * FROM %s %s", getTableName(), condition));
	}
	
	protected void throwSaveRepositoryException() throws RepositoryException {
		throw new RepositoryException(String.format("%s could not be saved", getTableName()));
	}
	
	protected void throwCreateTableRepositoryException() throws RepositoryException {
		throw new RepositoryException(String.format("Could not create table %s", getTableName()));
	}
	
	protected Integer getLastInsertId() throws SQLException {
		return getConnection().createStatement().executeQuery("SELECT last_insert_rowid() AS id LIMIT 1").getInt("id");
	}
}
