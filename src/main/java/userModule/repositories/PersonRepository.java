package userModule.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import userModule.models.Address;
import userModule.models.Person;
import userModule.models.Role;
import userModule.models.User;

public class PersonRepository extends Repository<Person> {
	private Connection connection;
	private Repository<User> userRepository;
	private Repository<Address> addressRepository;
	private Repository<Role> roleRepository;
	
	public PersonRepository(Connection connection, Repository<User> userRepository, Repository<Address> addressRepository, Repository<Role> roleRepository) {
		this.connection = connection;
		this.userRepository = userRepository;
		this.addressRepository = addressRepository;
		this.roleRepository = roleRepository;
	}

	public Integer persist(Person entity) throws RepositoryException {
		try (PreparedStatement statement = prepareInsert()) {
			statement.setInt(1, entity.getUserId());
			statement.setString(2, entity.getName());
			statement.setString(3, entity.getSurname());
			statement.setString(4, entity.getPhoneNumber());
			statement.executeQuery();
			return getLastInsertId();
		} catch (Exception e) {
			throwSaveRepositoryException();
		}
		return null;
	}

	public String getTableName() {
		return "Person";
	}

	public Connection getConnection() {
		return connection;
	}

	protected void createTableIfNotExists() throws RepositoryException {
		try (Statement statement = connection.createStatement()) {
			statement.executeUpdate(String.format("CREATE TABLE IF NOT EXISTS %s (id INTEGER PRIMARY KEY, user_id INTEGER, name VARCHAR, surname VARCHAR, phone VARCHAR, FOREIGN KEY(user_id) REFERENCES User(id))", getTableName()));
		} catch (SQLException ex) {
			throwCreateTableRepositoryException();
		}
	}

	protected Person buildEntity(ResultSet data) {
		Person person = new Person();
		try {
			person.setId(data.getInt("id"));
			person.setUser(userRepository.getById(data.getInt("user_id")));
			person.setName(data.getString("name"));
			person.setSurname(data.getString("surname"));
			person.setPhoneNumber(data.getString("phone"));
			String condition = String.format("person_id = %d", person.getId());
			person.setAddresses((ArrayList<Address>) addressRepository.getAllByCondition(condition));
			person.setRoles((ArrayList<Role>) roleRepository.getAllByCondition(condition));
		} catch (SQLException e) {
			// move along
		}
		return person;
	}
	
	private PreparedStatement prepareInsert() throws SQLException {
		return connection.prepareStatement(String.format("INSERT INTO %s(user_id, name, surname, phone) VALUES(?, ?, ?, ?)", getTableName()));
	}

}
