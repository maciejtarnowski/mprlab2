package userModule.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import userModule.models.User;

public class UserRepository extends Repository<User> {
	private Connection connection;
	
	public UserRepository(Connection connection) {
		this.connection = connection;
	}
	
	public Integer persist(User entity) throws RepositoryException {
		try (PreparedStatement statement = prepareInsert()) {
			statement.setString(1, entity.getLogin());
			statement.setString(2, entity.getPassword());
			statement.executeQuery();
			return getLastInsertId();
		} catch (Exception e) {
			throwSaveRepositoryException();
		}
		return null;
	}

	public String getTableName() {
		return "User";
	}

	public Connection getConnection() {
		return connection;
	}

	protected void createTableIfNotExists() throws RepositoryException {
		try (Statement statement = connection.createStatement()) {
			statement.executeUpdate(String.format("CREATE TABLE IF NOT EXISTS %s (id INTEGER PRIMARY KEY, login VARCHAR, password VARCHAR)", getTableName()));
		} catch (SQLException ex) {
			throwCreateTableRepositoryException();
		}
	}

	protected User buildEntity(ResultSet data) {
		User user = new User();
		try {
			user.setId(data.getInt("id"));
			user.setLogin(data.getString("login"));
			user.setPassword(data.getString("password"));
		} catch (SQLException e) {
			// move along
		}
		return user;
	}
	
	private PreparedStatement prepareInsert() throws SQLException {
		return connection.prepareStatement(String.format("INSERT INTO %s(login, password) VALUES(?, ?)", getTableName()));
	}
}
