package userModule.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import userModule.models.Permission;

public class PermissionRepository extends Repository<Permission> {
	private Connection connection;
	
	public PermissionRepository(Connection connection) {
		this.connection = connection;
	}

	public Integer persist(Permission entity) throws RepositoryException {
		try (PreparedStatement statement = prepareInsert()) {
			statement.setString(1, entity.getName());
			statement.setInt(2, entity.getRoleId());
			statement.executeQuery();
			return getLastInsertId();
		} catch (Exception e) {
			throwSaveRepositoryException();
		}
		return null;
	}
	
	protected void createTableIfNotExists() throws RepositoryException {
		try (Statement statement = connection.createStatement()) {
			statement.executeUpdate(String.format("CREATE TABLE IF NOT EXISTS %s (id INTEGER PRIMARY KEY, role_id INTEGER, name VARCHAR, FOREIGN KEY(role_id) REFERENCES Role(id))", getTableName()));
		} catch (SQLException ex) {
			throwCreateTableRepositoryException();
		}
	}
	
	protected Permission buildEntity(ResultSet data) {
		Permission permission = new Permission();
		try {
			permission.setId(data.getInt("id"));
			permission.setName(data.getString("name"));
			permission.setRoleId(data.getInt("role_id"));
		} catch (SQLException e) {
			// move along
		}
		return permission;
	}
	
	public String getTableName() {
		return "Permission";
	}
	
	public Connection getConnection() {
		return connection;
	}
	
	private PreparedStatement prepareInsert() throws SQLException {
		return connection.prepareStatement(String.format("INSERT INTO %s(name, role_id) VALUES(?, ?)", getTableName()));
	}
}
