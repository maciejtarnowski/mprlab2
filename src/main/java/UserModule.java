import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import userModule.repositories.AddressRepository;
import userModule.repositories.PermissionRepository;
import userModule.repositories.PersonRepository;
import userModule.repositories.RoleRepository;
import userModule.repositories.UserRepository;

public class UserModule {

	public static void main(String[] args) {
		try {
			Connection connection = DriverManager.getConnection("jdbc:sqlite::memory:");
			UserRepository userRepository = new UserRepository(connection);
			AddressRepository addressRepository = new AddressRepository(connection);
			PermissionRepository permissionRepository = new PermissionRepository(connection);
			RoleRepository roleRepository = new RoleRepository(connection, permissionRepository);
			PersonRepository personRepository = new PersonRepository(connection, userRepository, addressRepository, roleRepository);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
